using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class DesactivarTerodactilo : MonoBehaviour
{

    void Start()
    {
        
    }

    // Desactivar els pterodactils quan la seva posicio "x" es mes petita a -12
    void Update()
    {
        if (this.transform.position.x < -12)
        {
            this.gameObject.SetActive(false);
        }
    }



}
