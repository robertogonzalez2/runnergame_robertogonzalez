using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolTerodactil : MonoBehaviour
{

    public Velocidad vel_dificultad;
    void Start()
    {
        StartCoroutine(TerodactiloSpawnerPool());
    }

    
    void Update()
    {
        
    }


    IEnumerator TerodactiloSpawnerPool()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1, 10));
            bool act = true;
            for (int i = 0; i < this.transform.childCount; i++)
            {
                if (!this.transform.GetChild(i).gameObject.activeSelf && act == true)
                {
                    this.transform.GetChild(i).position = this.transform.position;
                    this.transform.GetChild(i).gameObject.SetActive(true);
                    this.transform.GetChild(i).GetComponent<Rigidbody2D>().velocity = new Vector2(vel_dificultad.velocidad * 1.2f, 0);
                    act = false;
                }
            }

        }
    }

}
