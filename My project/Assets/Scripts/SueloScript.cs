using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloScript : MonoBehaviour
{
    public Velocidad vel_dificultad;
    private float spritesize;
    void Start()
    {
        spritesize = (GetComponent<BoxCollider2D>().size.x) * 2;
    }

    // Gestiona el moviment del s�l i quan una mitad del sol ja no es visible, la passa a l'altra banda per generar l'efecte del s�l infinit
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel_dificultad.velocidad, 0);
        if (transform.position.x <= -spritesize)
            {
                transform.Translate(Vector2.right * spritesize * 2f);
                
            }
    }


}
